# Envdist

## What is it?
**Envdist** - is CLI tool for generation `.env` files from `.env.dist`.

**CLI Interface** `envdist [environment] [flag]`.

## Installation
```bash
$ npm i -g envdist
```
or
```bash
$ npm i -D envdist
```

You can set the package as global or dev dependency

Place | Command | Usage example
:--- | :--- |  :--- 
Global | `npm i -g envdist` | `$ envdist`
Dev  | `npm i -D envdist` | `$ npx envdist` <br><br>`$ ./node_modules/.bin/envdist `

## Usage

### Base usage

Create `.env.dist` file. For example.

```dotenv
#Application settings

NODE_ENV = <Environment>  # development
PORT = <Application port> # 3000
```

**Execute**

For manually enter (the environment is defined automatically from the `NODE_ENV` variable):
```bash
$ envdist
```

For manually enter variables of development environment:

```bash
$ envdist development
```

Generate file from environment variables (suitable for use in deploy or runnig CI/CD):

```bash
$ envdist production -e
```

Force generation (Sets default values else trying fetch from environment variables else set empty value):

```bash
$ envdist development -f
```

### Configuration
Insert this config to `package.json`.

```json
  "env": {
    "path": ".",
    "dist": ".env.dist",
    "pattern": ".env",
    "strategies": {
      "manual": ["", "dev"],
      "variables": ["prod", "stage", "test"]
    }
  }
```

**Options**

Option | Description
:--- |  :--- 
`path` | Path to your env.dist file. For example './env'. Default: '.'.
`dist` | Dist file name. If you have many dist files then you can use pattern '*.env.dist'. For environment test dist file will be `test.env.dist`. Default: '.env.dist'.
`pattern` | Pattern for output .env files. You can usage pattern as in `dist` option. Default: '.env'.
`strategies` | This parameter contains the `manual` and `variables` properties to declare the default strategy using the `NODE_ENV` variable.<br><br>`manual` equal run `envdist [ENV]` or `envdist [ENV] -m`.<br><br>`variables` equal run `envdist [ENV] -e`  

**Auto creation .env files after installation packages NPM**

Just add to your `package.json` next script

```json
{
  "scripts": {
    "install": "npx envdist"
  }
}
```

Triggered on `npm install` or `npm i`.


Enjoy!
