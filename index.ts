#!/usr/bin/env node
import { GenerateCommand } from './src/services';

new GenerateCommand().execute();
