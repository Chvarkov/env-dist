export enum Colors {
    Reset = "\x1b[0m",
    FgYellow = "\x1b[33m",
    FgRed = "\x1b[31m",
    FgGreen = "\x1b[32m",
    FgCyan = "\x1b[36m",
    FgWhite = "\x1b[37m",
}
