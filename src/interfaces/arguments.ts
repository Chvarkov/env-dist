import { StrategiesEnum } from '../enums/strategies.enum';

export interface IArguments {
    strategy: StrategiesEnum;
    environment: string;
}
