export interface IDistVariable {
    name: string;
    description: string;
    default?: string
}

export interface IEnvVariable {
    name: string;
    value: string;
}
