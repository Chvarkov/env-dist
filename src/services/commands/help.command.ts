import { AbstractCommand } from './command';

const BANNER =  '                    _ _     _   \n' +
                '  ___ _ ____   ____| (_)___| |_ \n' +
                ' / _ \\ \'_ \\ \\ / / _` | / __| __|\n' +
                '|  __/ | | \\ V / (_| | \\__ \\ |_ \n' +
                ' \\___|_| |_|\\_/ \\__,_|_|___/\\__|\n' +
                '                                ';

const USAGES = [
    'Usage: envdist [environment] [flag]',
    '       envdist [flag] [environment]',
    'Example: envdist develpment -m',
];

export class HelpCommand extends AbstractCommand {
    execute(): void {
        const content = [
            BANNER,
            USAGES.join('\n'),
            'Generation mode:',
            '-m - Manually enter variables. Default option.',
            '-f - Paste default values. If default value is not declared then paste empty value;',
            '-e - Paste values from environment variables;',
        ];

        this.console.writeLine(content.join('\n'));
        this.console.exit();
    }
}
