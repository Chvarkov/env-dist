import * as path from 'path';
import { AbstractStrategy } from './strategies';
import { IArguments, ISettings, IDistVariable, IEnvVariable } from '../../interfaces';
import { FileManager } from '../file-manager';
import { EnvParser } from '../parser';

export const DEFAULT_DIST = '.env.dist';
export const DEFAULT_PATH = '.';
export const DEFAULT_PATTERN = '.env';
export const ENV_VAR = 'NODE_ENV';

export class Generator {
    private readonly fileManager: FileManager;
    private readonly envParser: EnvParser;

    private existentVariables: IEnvVariable[] = [];

    constructor(private readonly strategy: AbstractStrategy) {
        this.fileManager = new FileManager();
        this.envParser = new EnvParser();
    }

    getStrategy(): AbstractStrategy {
        return this.strategy;
    }

    async generate(settings: ISettings, args: IArguments): Promise<void> {
        if (Array.isArray(settings.ignore) && settings.ignore.includes(process.env[ENV_VAR])) {
            return;
        }

        const currentPath = path.resolve(process.cwd(), settings.path || DEFAULT_PATH);
        const distFile  = settings.dist || DEFAULT_DIST;

        const fullFileName = path.resolve(currentPath, distFile);

        const lines = this.fileManager.read(fullFileName);

        let env = args.environment || process.env[ENV_VAR] || '';

        const outputFilename = (settings.pattern || DEFAULT_PATTERN).replace('*', env);

        if (this.fileManager.exist(currentPath, outputFilename)) {
            this.existentVariables = this.fileManager.read(path.resolve(currentPath, outputFilename))
                .map(line => this.envParser.parseEnv(line))
                .filter(variable => !!variable);
        }

        let content = '';
        for (const line of lines) {
            const variable = this.envParser.parseDist(line);

            if (!variable) {
                content += `${line}\n`;
                continue;
            }

            const existentVar = this.getExistentVariable(variable);

            if (existentVar) {
                const envLine = `${existentVar.name} = ${existentVar.value}`.trim();
                content += `${envLine}\n`;
                continue
            }

            if (variable.name === ENV_VAR) {
                env = env || variable.default || '';
                content += `${ENV_VAR} = ${env}\n`;
                continue;
            }

            const value = await this.strategy.generate(variable);
            content += `${value}\n`;
        }


        this.fileManager.save(currentPath, outputFilename, content);
    }

    private getExistentVariable(variable: IDistVariable): IEnvVariable | null {
        const result = this.existentVariables.filter(v => v.name === variable.name);

        return result.length === 1 ? result[0] : null;
    }
}
