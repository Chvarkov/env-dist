import { IDistVariable } from '../../../interfaces';
import { Console } from '../../console';

export abstract class AbstractStrategy {
    constructor(protected readonly console: Console) {
    }

    abstract generate(variable: IDistVariable): Promise<string>
}
