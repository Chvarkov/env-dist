import { AbstractStrategy } from './abstract.strategy';
import { IDistVariable } from '../../../interfaces';

export class ForceStrategy extends AbstractStrategy {
    async generate(variable: IDistVariable): Promise<string> {
        if (variable.default !== undefined) {
            return `${variable.name} = ${variable.default}`;
        }
        this.console.warning(`Warning! Variable '${variable.name}' is not defined.`);
        return `${variable.name} = `.trim();
    }
}
