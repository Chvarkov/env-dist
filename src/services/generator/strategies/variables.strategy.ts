import { AbstractStrategy } from './abstract.strategy';
import { IDistVariable } from '../../../interfaces';
import { ForceStrategy } from './force.strategy';

export class VariablesStrategy extends AbstractStrategy {
    async generate(variable: IDistVariable): Promise<string> {
        if (process.env[variable.name] === undefined) {
            return new ForceStrategy(this.console).generate(variable);
        }

        const value = process.env[variable.name] || variable.default || '';

        return `${variable.name} = ${value}`.trim();
    }
}
