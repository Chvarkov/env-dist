import { InvalidArgumentsException } from '../../exceptions';
import { IArguments } from '../../interfaces';
import { StrategiesEnum } from '../../enums';

const DEFAULT_STRATEGY = StrategiesEnum.Manual;

export class ArgumentParser {
    private readonly environmentRegex = /^[A-z]{1,255}$/m;
    private readonly flagRegex = /^(\-([fme]))?$/m;

    parse(args: string[]): IArguments {
        args = args.slice(2);

        if (args.length > 2) {
            throw new InvalidArgumentsException();
        }

        const environment = this.fetchEnvironment(args);
        const strategy = this.fetchStrategy(args);

        if (args.length) {
            throw new InvalidArgumentsException();
        }

        return {
            environment,
            strategy,
        };
    }

    private fetchStrategy(data: string[]): StrategiesEnum {
        if (data[0]) {
            const first = this.parseFlag(data[0]);
            if (first) {
                data.shift();
                return first as StrategiesEnum;
            }
        }

        return DEFAULT_STRATEGY;
    }

    private fetchEnvironment(data: string[]): string {
        if (data[0]) {
            const first = this.parseEnvironment(data[0]);

            if (first) {
                data.shift();
                return first;
            }

            if (data.length <= 1) {
                return process.env['NODE_ENV'];
            }
        }

        if (data[1]) {
            const last = this.parseEnvironment(data[1]);

            if (last) {
                data.pop();
                return last;
            }
        }

        return process.env['NODE_ENV'];
    }

    private parseEnvironment(value: string): string | null {
        const match = value.match(this.environmentRegex);

        if (!match || !match.length) {
            return null;
        }

        return match[0];
    }

    private parseFlag(value: string): string | null {
        const match = value.match(this.flagRegex);

        if (!match || !match.length) {
            return null;
        }

        return match[2];
    }
}
