export type ArgumentValue = string | boolean | undefined;

const TRUE = 'true';
const FALSE = 'false';

export interface IMap<T> {
    [key: string]: T;
}

export class ArgumentCollection {
    private readonly args: IMap<ArgumentValue> = {};
    private readonly flags = [];
    constructor(private readonly commandName: string) {
    }

    get(name: string): ArgumentValue {
        return this.args[name];
    }

    isExistsArgument(name: string): boolean {
        return !!this.args[name];
    }

    isExistsFlag(name: string): boolean {
        return this.flags.includes(name);
    }

    get command(): string {
        return this.commandName;
    }

    addFlag(name: string): this {
        this.flags.push(name);

        return this;
    }

    add(name: string, value: ArgumentValue): this {
        this.args[name] = value;

        return this;
    }
}

export class CommandParser {
    bool = [TRUE, FALSE];
    regex = {
        command: /^([A-z:]+)$/m,
        argumentName: /^--([A-z][A-z0-9-]*)$/m,
        quotedValue: /^'([^']*)'|"([^"]*)"|`([^`]*)`$/m,
        flag: /^-([A-z][A-z0-9-]*)$/m,
    };

    parse(line: string): ArgumentCollection {
        const segments = line.split(/[\s\t]+/m).map(ln => ln.trim()).filter(v => !!v);

        const commandName = segments.shift().toLowerCase();

        const matchCommand = commandName.match(this.regex.command);

        if (!matchCommand || matchCommand.length !== 2) {
            throw new Error('Invalid command');
        }

        const collection = new ArgumentCollection(commandName);

        for (const segment of segments) {
            const subSegments = segment.split('=');
            const name = subSegments.shift();

            const argNameMatch = name.match(this.regex.argumentName);

            if (argNameMatch && argNameMatch.length === 2) {
                if (subSegments.length === 1 && subSegments[0].length) {
                    const argValue = subSegments.join('=');

                    if (this.bool.includes(argValue)) {
                        collection.add(argNameMatch.pop(), argValue === TRUE);
                        continue;
                    }

                    const quotedValueMatch = argValue.match(this.regex.quotedValue);

                    if (quotedValueMatch && quotedValueMatch.length >= 2) {
                        collection.add(argNameMatch.pop(), quotedValueMatch.pop());
                        continue;
                    }

                    collection.add(argNameMatch.pop(), argValue);
                    continue;
                }

                collection.add(argNameMatch.pop(), true);
                continue;
            }

            const flagNameMatch = name.match(this.regex.flag);
            if (!flagNameMatch) {
                throw new Error(`Invalid argument ${name}`)
            }

            collection.addFlag(flagNameMatch.pop());
        }

        return collection;
    }
}

console.log(new CommandParser().parse('envdist --arg=val --arg2="fsdf" --arg2=`fsdf` --argBool --fal=false -f -d -g  '));
