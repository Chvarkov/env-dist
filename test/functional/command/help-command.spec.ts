import { HelpCommand } from '../../../src/services/commands';

describe('HelpCommand', () => {
    let helpCommand: HelpCommand;

    beforeAll(() => {
        helpCommand = new HelpCommand();
    });

    it('Method execute. Run command.', () => {
        helpCommand.execute();
    });
});
