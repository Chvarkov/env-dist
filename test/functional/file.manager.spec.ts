import { FileManager } from '../../src/services';
import * as path from 'path';

const TEST_CONTENT = 'Test passed!';

describe('FileManager', () => {
    let fileManager: FileManager;

    beforeAll(() => {
       fileManager = new FileManager();
    });

    it('Method save. Create file', () => {
        fileManager.save(path.join( __dirname, 'files'), 'test.txt', TEST_CONTENT);
    });

    it('Method exists. The file exists file.', () => {
        expect(fileManager.exist('.')).toBe(true);
    });

    it('Method exists. File does not exist.', () => {
        expect(fileManager.exist(new Date().toISOString())).toBe(false);
    });

    it('Method read. Read test file.', () => {
        const actualContent = fileManager.read(path.resolve(__dirname, 'files', 'test.txt'));

        expect(actualContent).toBeInstanceOf(Array);
        expect(actualContent.length).toBe(1);
        expect(actualContent.shift()).toBe(TEST_CONTENT);
    });
});
