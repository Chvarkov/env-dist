import { VariablesStrategy } from '../../../../src/services/generator/strategies';
import { Console } from '../../../../src/services';

const ENV_VARIABLE_NAME = 'ENV_TEST';
const ENV_VARIABLE_VALUE = 'test value';
const DEFAULT_VALUE = 'default test value';
const DESCRIPTION = 'test description';

const toLine = (name: string, value: string): string => `${name} = ${value}`.trim();

describe('VariablesStrategy', () => {
    let strategy: VariablesStrategy;



    beforeAll(() => {
        strategy = new VariablesStrategy(new Console());
    });

    it('Method generate. Value from environment variable.',  async () => {
        process.env[ENV_VARIABLE_NAME] = ENV_VARIABLE_VALUE;
        const value = await strategy.generate({
            name: ENV_VARIABLE_NAME,
            default: DEFAULT_VALUE,
            description: DESCRIPTION,
        });

        expect(value).toBe(toLine(ENV_VARIABLE_NAME, ENV_VARIABLE_VALUE));

        delete process.env[ENV_VARIABLE_NAME];
    });

    it('Method generate. Default value.',  async () => {
        const value = await strategy.generate({
            name: ENV_VARIABLE_NAME,
            default: DEFAULT_VALUE,
            description: DESCRIPTION,
        });

        expect(value).toBe(toLine(ENV_VARIABLE_NAME, DEFAULT_VALUE));
    });

    it('Method generate. Empty value.',  async () => {
        const value = await strategy.generate({
            name: ENV_VARIABLE_NAME,
            default: undefined,
            description: DESCRIPTION,
        });

        expect(value).toBe(toLine(ENV_VARIABLE_NAME, ''));
    });
});
