import { ArgumentParser } from '../../../src/services/parser';
import { StrategiesEnum } from '../../../src/enums';
import { InvalidArgumentsException } from '../../../src/exceptions';

const BASE_ARGS = ['bash', 'envdist'];
const ENVIRONMENT = 'dev';

describe('ArgumentParser', () => {
    let argumentParser: ArgumentParser;

    beforeAll(() => {
        argumentParser = new ArgumentParser();
    });

    it('Method parse. Without arguments.', () => {
        const args = argumentParser.parse(BASE_ARGS);
        expect(args.environment).toBe(process.env.NODE_ENV);
        expect(args.strategy).toBe(StrategiesEnum.Manual);
    });

    it('Method parse. Only environment.', () => {
        const args = argumentParser.parse([...BASE_ARGS, 'test']);
        expect(args.environment).toBe('test');
        expect(args.strategy).toBe(StrategiesEnum.Manual);
    });

    it('Method parse. Only manual strategy.', () => {
        const args = argumentParser.parse([...BASE_ARGS, '-m']);
        expect(args.environment).toBe(process.env.NODE_ENV);
        expect(args.strategy).toBe(StrategiesEnum.Manual);
    });

    it('Method parse. Only force strategy.', () => {
        const args = argumentParser.parse([...BASE_ARGS, '-f']);
        expect(args.environment).toBe(process.env.NODE_ENV);
        expect(args.strategy).toBe(StrategiesEnum.Force);
    });

    it('Method parse. Only variable strategy.', () => {
        const args = argumentParser.parse([...BASE_ARGS, '-e']);
        expect(args.environment).toBe(process.env.NODE_ENV);
        expect(args.strategy).toBe(StrategiesEnum.Variables);
    });

    it('Method parse. Environment and strategy.', () => {
        const args = argumentParser.parse([...BASE_ARGS, ENVIRONMENT, '-e']);
        expect(args.environment).toBe(ENVIRONMENT);
        expect(args.strategy).toBe(StrategiesEnum.Variables);
    });

    it('Method parse. Environment and strategy reverse.', () => {
        const args = argumentParser.parse([...BASE_ARGS, '-f', ENVIRONMENT]);
        expect(args.environment).toBe(ENVIRONMENT);
        expect(args.strategy).toBe(StrategiesEnum.Force);
    });

    it('Method parse. Invalid arguments count.', () => {
        const parse = () => argumentParser.parse([...BASE_ARGS, 'a', 'b', 'c']);
        expect(parse).toThrowError(InvalidArgumentsException);
    });

    it('Method parse. Invalid arguments.', () => {
        const parse = () => argumentParser.parse([...BASE_ARGS, 'a', 'b']);
        expect(parse).toThrowError(InvalidArgumentsException);
    });
});
