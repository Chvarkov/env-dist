import { EnvParser } from '../../../src/services/parser';

const NAME = 'APP_PORT';
const DESCRIPTION = 'Application port';
const DEFAULT = '3000';


describe('EnvParser', () => {
    let envParser: EnvParser;

    beforeAll(() => {
        envParser = new EnvParser();
    });

    it('Method parse. Static value.', () => {
        const variable = envParser.parseDist(`${NAME} = ${DEFAULT}`);
        expect(variable).toBeNull();
    });

    it('Method parse. Static value without spaces.', () => {
        const variable = envParser.parseDist(`${NAME}=${DEFAULT}`);
        expect(variable).toBeNull();
    });

    it('Method parse. Static value with comment.', () => {
        const variable = envParser.parseDist(`${NAME} = ${DEFAULT} # ${DESCRIPTION}`);
        expect(variable).toBeNull();
    });

    it('Method parse. Static value with comment without spaces.', () => {
        const variable = envParser.parseDist(`${NAME}=${DEFAULT}#${DESCRIPTION}`);
        expect(variable).toBeNull();
    });

    it('Method parse. Variable without default value.', () => {
        const variable = envParser.parseDist(`${NAME} = <${DESCRIPTION}>`);
        expect(variable.name).toBe(NAME);
        expect(variable.description).toBe(DESCRIPTION);
        expect(variable.default).toBeUndefined();
    });

    it('Method parse. Variable without default value and spaces .', () => {
        const variable = envParser.parseDist(`${NAME}=<${DESCRIPTION}>`);
        expect(variable.name).toBe(NAME);
        expect(variable.description).toBe(DESCRIPTION);
        expect(variable.default).toBeUndefined();
    });

    it('Method parse. Variable with default value.', () => {
        const variable = envParser.parseDist(`${NAME} = <${DESCRIPTION}> # ${DEFAULT}`);
        expect(variable.name).toBe(NAME);
        expect(variable.description).toBe(DESCRIPTION);
        expect(variable.default).toBe(DEFAULT);
    });

    it('Method parse. Variable without spaces but with default value.', () => {
        const variable = envParser.parseDist(`${NAME}=<${DESCRIPTION}> # ${DEFAULT}`);
        expect(variable.name).toBe(NAME);
        expect(variable.description).toBe(DESCRIPTION);
        expect(variable.default).toBe(DEFAULT);
    });
});
