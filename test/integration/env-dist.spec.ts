import * as path from 'path';
import * as child_process from 'child_process';
import { FileManager } from '../../src/services';

const TEST_PACKAGE_DIR = path.resolve(__dirname, 'test-package');
const ENV_VAR_NAME = 'API_SERVICE_2_ACCESS_TOKEN';
const ENV_VAR_VALUE = 'token_access_for_api_service_2_12345567890!@#$%^&*()_-+=/\\<>';

describe('envdist', () => {
    let fileManager: FileManager;

    beforeAll(() => {
        fileManager = new FileManager();
        child_process.execSync(`tsc && cd test/integration/test-package && npm i`);
        process.env[ENV_VAR_NAME] = ENV_VAR_VALUE;
    });

    afterAll(() => {
        delete process.env[ENV_VAR_NAME];
    });

    it('Execute envdist without env',  async () => {
        child_process.execSync(`cd test/integration/test-package && ${ENV_VAR_NAME}="${ENV_VAR_VALUE}" npx envdist -e`);

        const dir = path.resolve(TEST_PACKAGE_DIR, 'env');
        expect(fileManager.exist(dir, 'result-test.env')).toBe(true);

        const actualResult = fileManager.read(path.resolve(dir, 'result-test.env'));
        const expectedResult = fileManager.read(path.resolve(TEST_PACKAGE_DIR, 'expected-test.env'));
        expect(actualResult).toStrictEqual(expectedResult);
    });

    it('Execute envdist with env',  async () => {
        child_process.execSync('cd test/integration/test-package && npx envdist dev -f');

        const dir = path.resolve(TEST_PACKAGE_DIR, 'env');
        expect(fileManager.exist(dir, 'result-dev.env')).toBe(true);

        const actualResult = fileManager.read(path.resolve(dir, 'result-dev.env'));
        const expectedResult = fileManager.read(path.resolve(TEST_PACKAGE_DIR, 'expected-dev.env'));
        expect(actualResult).toStrictEqual(expectedResult);
    });
});
